# Bree

Bree is a [Hugo](http://gohugo.io) theme used for the [Bitflip Research blog](https://br.ee).

Bree is heavily based on the [Ed](https://github.com/sergeyklay/gohugo-theme-ed) Hugo theme.

## License

Ed licensed under the MIT License. See the [LICENSE](LICENSE) file for more information.
